package modelDAO;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

import model.User;
import utilDAO.GenericDAO;

public class UserDAO extends GenericDAO<User> {

	public UserDAO(EntityManager em) {
		super(em);
	}

	public Long countByLogin(User u) {
		try {
			String consulta = "SELECT count(u) FROM User u WHERE u.login = ?1";
			Query q1 = getEntityManager().createQuery(consulta);
			q1.setParameter(1, u.getLogin());
			return (Long) q1.getSingleResult();

		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a busca do usu�rio no banco de dados!");
		}
	}

	public User selectByLoginPassword(User u) {
		try {
			String consulta = "SELECT u FROM User u WHERE u.login = ?1 AND u.password = ?2";
			Query q1 = getEntityManager().createQuery(consulta, User.class);
			q1.setParameter(1, u.getLogin());
			q1.setParameter(2, u.getPassword());
			return (User) q1.getSingleResult();

		} catch (NoResultException e) {
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public User selectLast(User user) {
		try {
			String query = "SELECT u FROM User u ORDER BY u.id DESC";
			Query q1 = getEntityManager().createQuery(query, User.class);
			q1.setMaxResults(1);
			return (User) q1.getSingleResult();

		} catch (PersistenceException pe) {
			pe.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a verifica��o do usu�rio no banco de dados!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenceException("Erro ocorreu durante a verifica��o do usu�rio no banco de dados!");
		}
	}
}