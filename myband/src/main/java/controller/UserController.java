package controller;

import model.User;
import modelDAO.UserDAO;
import utilDAO.FactoryDAO;

public class UserController {

	private UserDAO userDAO;

	public UserController() {
		userDAO = FactoryDAO.getUserDAO();
	}

	public User login(User u) {
		if (userDAO.countByLogin(u) > 0) {
			User user = userDAO.selectByLoginPassword(u);

			if (user != null) {
				user.setStatusCode(User.loginOk);
				return user;
			} else {
				u.setStatusCode(User.userAndPasswordDoesntMatch);
				return u;
			}

		} else {
			u.setStatusCode(User.userDoesntExist);
			return u;
		}
	}

	public User createUser(User user) {
		if (userDAO.countByLogin(user) > 0) {
			user.setStatusCode(User.userAlreadyExists);
			return user;
		} else {
			user.setLatitude(-8.151551);
			user.setLongitude(-34.919925);
			userDAO.insert(user);
			user = userDAO.selectLast(user);
			user.setStatusCode(User.loginOk);
			return user;
		}
	}

	public User updateUser(User user) {
		User u = userDAO.selectByLoginPassword(user);
		if (u == null) {
			user.setStatusCode(User.userAndPasswordDoesntMatch);
			return user;
		} else {
			user = userDAO.alter(user);
			user.setStatusCode(User.updateOk);
			return user;
		}
	}

	public User deleteUser(User user) {
		User u = userDAO.selectByLoginPassword(user);
		if (u == null) {
			user.setStatusCode(User.userAndPasswordDoesntMatch);
			return user;
		} else {
			userDAO.delete(user);
			user.setStatusCode(User.deleteOk);
			return user;
		}
	}
}
