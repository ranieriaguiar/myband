package rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import controller.UserController;
import model.User;

@Path("/user")
public class UserResources {

	@POST
	@Path("/login")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response login(User user) {
		UserController userController = new UserController();
		user = userController.login(user);
		return Response.ok(user).build();
	}

	@POST
	@Path("/createuser")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response createUser(User user) {
		UserController userController = new UserController();
		user = userController.createUser(user);
		return Response.ok(user).build();
	}

	@POST
	@Path("/updateuser")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateUser(User user) {
		UserController userController = new UserController();
		user = userController.updateUser(user);
		return Response.ok(user).build();
	}

	@POST
	@Path("/deleteuser")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Response deleteUser(User user) {
		UserController userController = new UserController();
		user = userController.deleteUser(user);
		return Response.ok(user).build();
	}

}
