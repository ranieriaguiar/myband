package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@XmlRootElement(name = "listRating")
public class Rating implements Serializable {
	private static final long serialVersionUID = 1L;

	public Rating() {
	}

	@Id
	@Column(name = "user_service")
	private Long id;

	@Column(nullable = false)
	private boolean requester;

	@Column(nullable = false)
	private int rating;

	@Column(length = 80, nullable = false)
	private String description;

	@OneToOne
	@Cascade(CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private Event event;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isRequester() {
		return requester;
	}

	public void setRequester(boolean requester) {
		this.requester = requester;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Event getService() {
		return event;
	}

	public void setService(Event event) {
		this.event = event;
	}

}
