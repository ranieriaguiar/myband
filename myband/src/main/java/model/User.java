package model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
@Table(name = "users")
@XmlRootElement(name = "listUser")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int loginOk = 1;
	public static final int userAndPasswordDoesntMatch = 2;
	public static final int userDoesntExist = 3;
	public static final int userAlreadyExists = 4;
	public static final int serverError = 5;
	public static final int updateOk = 6;
	public static final int deleteOk = 7;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 50, nullable = false)
	private String userName;

	@Column(length = 15, nullable = false, unique = true)
	private String login;

	@Column(length = 15, nullable = false)
	private String password;

	@Column(length = 15, nullable = false)
	private Double latitude;

	@Column(length = 15, nullable = false)
	private Double longitude;

	@Transient
	private int statusCode;

	@OneToOne
	@Cascade(CascadeType.DELETE)
	@PrimaryKeyJoinColumn
	private UserDescription userDescription;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idCategory", insertable = true, updatable = true)
	@Fetch(FetchMode.JOIN)
	private Category category;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	private List<UserAvailability> listUserAvailability;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	private List<UserVideos> listUserVideos;

	@OneToMany(mappedBy = "provider", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	private List<Event> listEventsProvided;

	@OneToMany(mappedBy = "requester", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	private List<Event> listEventsRequested;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "userGenre", joinColumns = @JoinColumn(name = "idUser"), inverseJoinColumns = @JoinColumn(name = "idSubGenre"))
	private List<SubGenre> listSubGenre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserDescription getUserDescription() {
		return userDescription;
	}

	public void setUserDescription(UserDescription userDescription) {
		this.userDescription = userDescription;
	}

	public List<UserAvailability> getListUserAvailability() {
		return listUserAvailability;
	}

	public void setListUserAvailability(List<UserAvailability> listUserAvailability) {
		this.listUserAvailability = listUserAvailability;
	}

	public List<UserVideos> getListUserVideos() {
		return listUserVideos;
	}

	public void setListUserVideos(List<UserVideos> listUserVideos) {
		this.listUserVideos = listUserVideos;
	}

	public List<Event> getListEventsProvided() {
		return listEventsProvided;
	}

	public void setListEventsProvided(List<Event> listEventsProvided) {
		this.listEventsProvided = listEventsProvided;
	}

	public List<Event> getListEventsRequested() {
		return listEventsRequested;
	}

	public void setListEventsRequested(List<Event> listEventsRequested) {
		this.listEventsRequested = listEventsRequested;
	}

	public List<SubGenre> getListSubGenre() {
		return listSubGenre;
	}

	public void setListSubGenre(List<SubGenre> listSubGenre) {
		this.listSubGenre = listSubGenre;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
}
