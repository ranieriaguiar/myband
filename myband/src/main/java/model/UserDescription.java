package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "listUserDescription")
public class UserDescription implements Serializable {
	private static final long serialVersionUID = 1L;

	public UserDescription() {
	}

	@Id
	@Column(name = "user_id")
	private Long id;

	@Column(length = 50, nullable = false)
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}