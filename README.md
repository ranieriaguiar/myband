Este foi um projeto de faculdade que consiste em uma comunicação entre uma API e um aplicativo Android nativo.

A API foi desenvolvida utilizando a linguagem Java, Hibernate para persistência de dados e a implementação Jersey para a arquitetura RESTful. Para a base de dados foi utilizado o PostgreSQL.

Repositório do aplicativo: https://github.com/mybandmobile/MyBand

PS.: a API foi executada em um Raspberry PI, mas por utilizar a linguagem Java, pode ser executada em outros ambientes.